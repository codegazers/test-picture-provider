package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"
)

// PictureDir is the directory the picture will be saved in
const PictureDir = "./pictures"

// DefaultMaxPictures is the  number of pictures the app will download to cache
const DefaultMaxPictures = 100

// DefaultPictureURL is the url the test pictures will be downloaded from
const DefaultPictureURL = "https://picsum.photos/600/480"

var config = struct {
	pictureInfos []os.FileInfo
	maxPictures  int
	pictureURL   string
}{}

func init() {
	fmt.Println("Check dirs")
	if _, err := os.Stat(PictureDir); err != nil {
		panic("Dir \"" + PictureDir + "\" does not exist")
	}

	log.Println("Load env")
	loadEnv()
	log.Println("Load picture data")
	loadPictureInfo()
	log.Println("Download missing pictures")
	downloadRemainingPictures()
	log.Println("Reload picture data")
	loadPictureInfo()
}

func loadPictureInfo() {
	fileInfos, err := ioutil.ReadDir(PictureDir)
	if err != nil {
		panic("Error while reading \"" + PictureDir + "\"")
	}

	for _, fileInfo := range fileInfos {
		if fileInfo.Name() != ".gitignore" {
			config.pictureInfos = append(config.pictureInfos, fileInfo)
		}
	}
	log.Printf("Current number of pictures: %d", len(config.pictureInfos))
}

func loadEnv() {
	envMaxPictures := os.Getenv("MAX_NUMBER_OF_PICTURES")
	if len(envMaxPictures) > 0 {
		if maxPictures, err := strconv.Atoi(os.Getenv("MAX_NUMBER_OF_PICTURES")); err != nil {
			panic("\"" + envMaxPictures + "\" cou not be parsed to int")
		} else {
			config.maxPictures = maxPictures
		}
	} else {
		config.maxPictures = DefaultMaxPictures
	}

	if envPictureURL := os.Getenv("MAX_PICTURE_SOURCE_URL"); len(envPictureURL) > 0 {
		config.pictureURL = envPictureURL
	} else {
		config.pictureURL = DefaultPictureURL
	}
}

func downloadRemainingPictures() {
	log.Printf("Missing pictures: %d", config.maxPictures-len(config.pictureInfos))
	for i := len(config.pictureInfos); i < config.maxPictures; i++ {
		src, err := http.Get(config.pictureURL)
		if err != nil {
			panic("Unable to fetch image from \"" + config.pictureURL + "\": " + err.Error())
		}
		defer src.Body.Close()

		picturePath := PictureDir + "/" + strconv.Itoa(time.Now().Nanosecond()) + ".jpg"
		newImage, err := os.Create(picturePath)
		if err != nil {
			panic("Cannot create file in \"" + PictureDir + "\": " + err.Error())
		}

		_, err = io.Copy(newImage, src.Body)
		if err != nil {
			panic("Error while saving file from url to file: " + err.Error())
		}

		if err := os.Chmod(picturePath, 0777); err != nil {
			panic("Cannot set permissions of \"" + picturePath + "\" to 0777")
		}
		log.Printf("New picture downloaded. %d more pictures need to be downloaded", config.maxPictures-i-1)
	}
}

func main() {
	log.Println("Start webserver")
	http.HandleFunc("/status", handleStatus)
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":80", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	rand.Seed(time.Now().UnixNano())

	picturePath := PictureDir + "/" + config.pictureInfos[rand.Intn(config.maxPictures)].Name()

	picture, err := os.Open(picturePath)
	if err != nil {
		http.Error(w, "Picture not found (please restart the container)", 500)
		return
	}

	io.Copy(w, picture)
}

func handleStatus(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "app is running")
}
